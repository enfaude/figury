package figury;

import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class AnimatorApp extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private boolean puste = true;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					final AnimatorApp frame = new AnimatorApp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param delay 
	 */
	public AnimatorApp() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int ww = 450, wh = 300;
		setBounds((screen.width-ww)/2, (screen.height-wh)/2, ww, wh);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);

		AnimPanel kanwa = new AnimPanel();
		kanwa.setBounds(0, 0, 440, 228);
		kanwa.setBackground(Color.white);
		contentPane.add(kanwa);
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				kanwa.initialize();
			}
		});

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				kanwa.addFig();
				if (puste == true) {
					kanwa.animate();
					puste = false;
					}
			}
		});
		btnAdd.setBounds(5, kanwa.getHeight()+4, 80, 20);
		contentPane.add(btnAdd);
		
		JButton btnAnimate = new JButton("Animate");
		//dop�ki nie ma �adnych figur, to przycisk Animate nic nie robi
		btnAnimate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (puste == false) {
				kanwa.animate();
				}
			}
		});
		btnAnimate.setBounds(90, kanwa.getHeight()+4, 90, 20);
		contentPane.add(btnAnimate);		
		
		contentPane.addComponentListener(new ComponentListener()
		{
		@Override
		public void componentHidden(ComponentEvent arg0) {}
					
		@Override
		public void componentMoved(ComponentEvent arg0) {}

		@Override
		public void componentResized(ComponentEvent arg0) {
			kanwa.setBounds(0, 0, contentPane.getWidth(), contentPane.getHeight()-25);
			kanwa.continueOnResize();
			btnAdd.setBounds(5, kanwa.getHeight()+4, 80, 20);
			btnAnimate.setBounds(90, kanwa.getHeight()+4, 90, 20);
		}
					
		@Override
		public void componentShown(ComponentEvent arg0) {}
		});
	}
}